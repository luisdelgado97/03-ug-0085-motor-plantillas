
CREATE TABLE IF NOT EXISTS "Integrantes" (
	"matricula" TEXT NOT NULL UNIQUE,
	"nombre" TEXT,
	" apellido" TEXT,
	"rol" TEXT,
	"codigo" INTEGER,
	"url" TEXT,
	"esta_borrado" INTEGER,
	"orden" INTEGER,
	PRIMARY KEY("matricula")	
);

CREATE TABLE IF NOT EXISTS "tiposMedio" (
	"tipo" TEXT NOT NULL UNIQUE,
	"descripcion" TEXT,
	"esta_borrado" INTEGER,
	"orden" INTEGER,
	PRIMARY KEY("tipo")	
);

CREATE TABLE IF NOT EXISTS "Media" (
	"id" INTEGER NOT NULL UNIQUE,
	"matricula" TEXT,
	"url" TEXT,
	"titulo" TEXT,
	"parrafo" TEXT,
	"imagen" TEXT,
	"video" TEXT,
	"html" TEXT,
	"tipos_medio" TEXT,
	"esta_borrado" INTEGER,
	"orden" INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY ("matricula") REFERENCES "Integrantes"("matricula")
	ON UPDATE NO ACTION ON DELETE NO ACTION
);
