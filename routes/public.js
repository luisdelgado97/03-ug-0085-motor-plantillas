//archivos que contiene todas las rutass publicas de mi proyecto
require("dotenv").config();
const db = require( '../db/data' );
const express = require("express");
const router = express.Router();

//........................Direcciones....................

// Definir variables de entorno
const NOMBRE =process.env.NOMBRE;
const APELLIDO =process.env.APELLIDO;
const SITIO =process.env.SITIO;
const MATERIA =process.env.MATERIA;
//Directorio de index
// Al recibir un request a la direccion "/" se renderiza (muestra) el archivo "index"de la carpeta
router.get("/", (request, response) => {
    console.log("Nombre:", process.env.NOMBRE);
    console.log("Apellido:", process.env.APELLIDO);
    console.log("Sitio:", process.env.SITIO);
    console.log("Materia:", process.env.MATERIA);



    const matriculaDeHome = "Home";
    const Home = [];
    for (indice in db.media) {
        if (matriculaDeHome === db.media[indice].matricula)
            Home.push(db.media[indice]);
    }
    response.render("index", {
        data: Home,
        base_url: "http://localhost:8080",
        integrantes: db.integrantes,
        nombre: NOMBRE,
        apellido: APELLIDO,
        sitio: SITIO,
        materia: MATERIA,
    });
});
router.get("/integrantes/:matricula", (request, response) => {
    const matricula = request.params.matricula;

    let integrante = {};
    for (let indice in db.integrantes) {
        if (matricula === db.integrantes[indice].matricula) {
            integrante = db.integrantes[indice];
        }
    }
    const datos = [];
    for (let indice in db.media) {
        if (matricula === db.media[indice].matricula) {
            datos.push(db.media[indice]);
        }
    }

    if (Object.keys(integrante).length === 0) {
        response.status(404).render("error");
    } else {
        response.render("integrante", {
            data: datos,
            base_url: "http://localhost:8080",
            integrantes: db.integrantes,
            nombre: NOMBRE,
            apellido: APELLIDO,
            sitio: SITIO,
            materia: MATERIA,
            

        });
    }
});

router.get("/paginas/word_cloud.html", (request, response) => {
    response.render("word_cloud",{
        integrantes: db.integrantes,
        base_url: "http://localhost:8080",
        nombre: NOMBRE,
        apellido: APELLIDO,
        sitio: SITIO,
        materia: MATERIA,
            
    });
});
router.get("/paginas/curso.html", (request, response) => {
    response.render("curso",{
        integrantes: db.integrantes,
        base_url: "http://localhost:8080",
        nombre: NOMBRE,
        apellido: APELLIDO,
        sitio: SITIO,
         materia: MATERIA,
            
    });
    
});

router.use ((request, response, next)=>{
    response.status(404).render ('error');
});




//exportamos el router
module.exports = router;
